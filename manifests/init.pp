class aws_cli {
  exec { 'install':
    command => '/usr/local/bin/pip install awscli',
    unless => 'test -f /usr/local/bin/aws > /dev/null 2>&1'
  }
}